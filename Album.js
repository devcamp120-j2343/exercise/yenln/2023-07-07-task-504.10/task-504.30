import Product from "./Product.js";
class Album extends Product{
    constructor(name,price,number,artist){
        super(name,price,number);
        this.artist = artist;
    }
    sellCopies(){
        console.log(this.name);
        console.log(this.price);
        console.log(this.number);
        console.log(this.artist);
    }
}

export default Album;