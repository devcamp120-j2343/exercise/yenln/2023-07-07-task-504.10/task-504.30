import Product from "./Product.js";
class Movie extends Product{
    constructor(name,price,number,director){
        super(name,price,number);
        this.director = director;
    }
    sellCopies(){
        console.log(this.name);
        console.log(this.price);
        console.log(this.number);
        console.log(this.director);
    }
}

export default Movie;