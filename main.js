import Product from "./Product.js";
import Album from "./Album.js";
import Movie from "./movie.js";
var product1 = new Product("Coke", 800, 1000);
console.log(product1.sellCopies());
console.log(product1 instanceof Product);

var album1 = new Album("Coke", 800, 1000, "zDs");
console.log(album1.sellCopies());
console.log(album1 instanceof Album);

var movie1 = new Movie("Coke", 800, 1000, "Bac");
console.log(movie1.sellCopies());
console.log(album1 instanceof Movie);
